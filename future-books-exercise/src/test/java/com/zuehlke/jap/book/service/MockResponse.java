package com.zuehlke.jap.book.service;

public class MockResponse implements BookResponse {

	public String json;
	public String notFoundMsg;
	public String unavailableMsg;

	@Override
	public void ok(String json) {
		this.json = json;
	}

	@Override
	public void notFound(String message) {
		this.notFoundMsg = message;
	}

	@Override
	public void unavailable(String message) {
		this.unavailableMsg = message;
	}

}
