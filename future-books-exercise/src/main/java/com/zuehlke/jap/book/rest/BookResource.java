package com.zuehlke.jap.book.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.MediaType;

import com.zuehlke.jap.book.service.BookService;
import com.zuehlke.jap.book.service.SimpleBookService;

/**
 * Root book resource.
 * TODO Timeout
 */
@Path("books")
public class BookResource {

	BookService bookService = new SimpleBookService();

	@GET
    @Produces(MediaType.APPLICATION_JSON)
    public void getAllBooks(@Suspended AsyncResponse response) {
        bookService.allBooks(new AsyncBookResponse(response));
    }
    
    @GET
    @Path("{bookId}")
    @Produces(MediaType.APPLICATION_JSON)
    public void getBookById(@PathParam("bookId") String bookId,
    		                @Suspended AsyncResponse response) {
    	long id = Long.parseLong(bookId);
    	bookService.bookById(id, new AsyncBookResponse(response));
    }
}
