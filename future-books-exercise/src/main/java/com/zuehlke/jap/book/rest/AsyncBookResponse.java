package com.zuehlke.jap.book.rest;

import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.Response;

import com.zuehlke.jap.book.service.BookResponse;

public class AsyncBookResponse implements BookResponse {

	private final AsyncResponse response;

	public AsyncBookResponse(AsyncResponse response) {
		this.response = response;
	}

	@Override
	public void ok(String json) {
		response.resume(json);
	}

	@Override
	public void notFound(String message) {
		Response value = Response.status(Response.Status.NOT_FOUND).entity(message).build();
		response.resume(value);
	}

	@Override
	public void unavailable(String message) {
		Response value = Response.status(Response.Status.SERVICE_UNAVAILABLE).entity(message).build();
		response.resume(value);
	}

}
