package com.zuehlke.jap.book.integration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

public class BookAvailability {
	
	private static final String PATH = "com/zuehlke/jap/book/integration/";
	private static final String FILENAME = "availability.txt";
	private static final String ENCODING = "utf8";

	private BufferedReader getReader() throws IOException {
		InputStream in = this.getClass().getClassLoader().getResourceAsStream(PATH + FILENAME);
		if (in == null) throw new IOException("Missing file " + FILENAME);
		Reader reader = new InputStreamReader(in, ENCODING);
		return new BufferedReader(reader);
	}

	public Availability availabilityByBook(long id) throws IOException {
		try (BufferedReader reader = getReader()) {
			while (reader.ready()) {
				String line = reader.readLine();
				Availability available = parse(line);
				if (available.getId() == id) return available;
			}
			return new Availability(id, 0);
		}
	}
	
	public Map<Long,Availability> readAllAvailabilities() throws IOException {
		try (BufferedReader reader = getReader()) {
			Map<Long,Availability> availables = new HashMap<Long,Availability>();
			while (reader.ready()) {
				String line = reader.readLine();
				final Availability available = parse(line);
				availables.put(available.getId(), available);
			}
			return availables;
		}
	}
	
	private Availability parse(String line) {
		String[] items = line.split("\t");
		return new Availability(Long.parseLong(items[0]), Integer.parseInt(items[1]));
	}

}
