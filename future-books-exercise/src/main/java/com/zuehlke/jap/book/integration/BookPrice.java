package com.zuehlke.jap.book.integration;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

public class BookPrice {
	
	private static final String PATH = "com/zuehlke/jap/book/integration/";
	private static final String FILENAME = "price.txt";
	private static final String ENCODING = "utf8";

	private BufferedReader getReader() throws IOException {
		InputStream in = this.getClass().getClassLoader().getResourceAsStream(PATH + FILENAME);
		if (in == null) throw new IOException("Missing file " + FILENAME);
		Reader reader = new InputStreamReader(in, ENCODING);
		return new BufferedReader(reader);
	}

	public Price priceByBook(long id) throws IOException {
		try (BufferedReader reader = getReader()) {
			while (reader.ready()) {
				String line = reader.readLine();
				Price price = parse(line);
				if (price.getId() == id) return price;
			}
			return new Price(id, 0.0f);
		}
	}
	
	public Map<Long,Price> readAllPrices() throws IOException {
		try (BufferedReader reader = getReader()) {
			Map<Long,Price> prices = new HashMap<Long,Price>();
			while (reader.ready()) {
				String line = reader.readLine();
                final Price price = parse(line);
                prices.put(price.getId(), price);
			}
			return prices;
		}
	}
	
	private Price parse(String line) {
		String[] items = line.split("\t");
		return new Price(Long.parseLong(items[0]), Float.parseFloat(items[1]));
	}

}
