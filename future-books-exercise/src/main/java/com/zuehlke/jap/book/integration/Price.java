package com.zuehlke.jap.book.integration;

public class Price {

    private final long id;
    private final float price;

    public Price(long id, float price) {
        this.id = id;
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public float getPrice() {
        return price;
    }
}
