package com.zuehlke.jap.book.service;

import akka.dispatch.Mapper;
import akka.dispatch.OnFailure;
import akka.dispatch.OnSuccess;
import akka.japi.Option;
import com.zuehlke.jap.book.integration.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import scala.concurrent.Future;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.Callable;

public class Java7BookService implements BookService {

	protected static Logger LOG = LoggerFactory.getLogger(Java7BookService.class);
	
	protected BookCatalog catalog = new BookCatalog();
	protected BookAvailability available = new BookAvailability();
    protected BookPrice pricing = new BookPrice();
	protected FutureDispatcher dispatcher = new FutureDispatcher();

    /**
     * Get all books, complemented by availability and price.
     *
	 * PROBLEM: Callbacks with Java7 make code unreadable, too much boiler-plate.
	 * PROBLEM: Executor needs to be used everywhere -> Dispatcher
	 */
	public void allBooks(final BookResponse response) {
        Future<Collection<Book>> books = readBooks(response);
        Future<Map<Long,Availability>> availability = readAvailability(response);
        Future<Map<Long,Price>> prices = readPrices(response);
        Future<Collection<BookOffer>> offers = mergeBookOffer(books, availability, prices);
        Future<String> json = convertToJson(offers);
        handleResult(json, response);
        LOG.info("allBooks requested");
	}

    protected Future<Collection<Book>> readBooks(final BookResponse response) {
        Future<Collection<Book>> books = dispatcher.future(new Callable<Collection<Book>>() {
            @Override
            public Collection<Book> call() throws IOException {
                LOG.info("reading books");
                return catalog.readAllBooks();
            }
        });
        dispatcher.onFailure(books, new OnFailure(){
            @Override
            public void onFailure(Throwable ex) throws Throwable {
                response.unavailable(ex.getMessage());
                LOG.error("Failed reading all books", ex);
            }
        });
        return books;
    }

    protected Future<Map<Long,Availability>> readAvailability(final BookResponse response) {
        Future<Map<Long,Availability>> availability = dispatcher.future(new Callable<Map<Long,Availability>>(){
            @Override
            public Map<Long,Availability> call() throws Exception {
                LOG.info("reading availability");
                return available.readAllAvailabilities();
            }
        });
        dispatcher.onFailure(availability, new OnFailure(){
            @Override
            public void onFailure(Throwable ex) throws Throwable {
                response.unavailable(ex.getMessage());
                LOG.error("Failed reading availability", ex);
            }
        });
        return availability;
    }

    protected Future<Map<Long,Price>> readPrices(final BookResponse response) {
        Future<Map<Long,Price>> prices = dispatcher.future(new Callable<Map<Long,Price>>(){
            @Override
            public Map<Long,Price> call() throws Exception {
                LOG.info("reading prices");
                return pricing.readAllPrices();
            }
        });
        dispatcher.onFailure(prices, new OnFailure(){
            @Override
            public void onFailure(Throwable ex) throws Throwable {
                response.unavailable(ex.getMessage());
                LOG.error("Failed reading prices", ex);
            }
        });
        return prices;
    }

    protected Future<Collection<BookOffer>> mergeBookOffer(Future<Collection<Book>> books, Future<Map<Long,Availability>> availability, Future<Map<Long,Price>> prices) {
        return dispatcher.zip(books, availability, prices, new FutureDispatcher.Zipper3Callback<Collection<Book>, Map<Long,Availability>, Map<Long,Price>, Collection<BookOffer>>(){
            @Override
            public Collection<BookOffer> map(Collection<Book> books, Map<Long,Availability> availability, Map<Long,Price> prices) {
                LOG.info("combine books and availability");
                return BookOffer.merge(books, availability, prices);
            }
        });
    }

    protected Future<String> convertToJson(Future<Collection<BookOffer>> offers) {
        return dispatcher.map(offers, new Mapper<Collection<BookOffer>,String>(){
            @Override
            public String apply(Collection<BookOffer> offers) {
                LOG.info("book offers to JSON");
                return BookOffer.toJson(offers);
            }
        });
    }

    protected void handleResult(Future<String> json, final BookResponse response) {
        dispatcher.onSuccess(json, new OnSuccess<String>() {
            @Override
            public void onSuccess(String json) {
                response.ok(json);
                LOG.info("allBooks responded");
            }
        });
    }

    /**
     * Get a single book by its id, complemented by availability and price.
     */
    public void bookById(long id, BookResponse response) {
        final Future<Option<Book>> book = readBookById(id, response);
        final Future<Availability> availability = readAvailabilityById(id, response);
        final Future<Price> price = readPriceById(id, response);
        final Future<Option<BookOffer>> offer = mergeBookOffer(book, availability, price, response);
        handleResult(id, offer, response);
	}

    protected Future<Option<Book>> readBookById(final long id, final BookResponse response) {
        final Future<Option<Book>> book = dispatcher.future(new Callable<Option<Book>>() {
            @Override
            public Option<Book> call() throws Exception {
                LOG.info("reading book with id {}", id);
                return catalog.readById(id);
            }
        });
        dispatcher.onFailure(book, new OnFailure() {
            @Override
            public void onFailure(Throwable ex) {
                response.unavailable(ex.getMessage());
                LOG.error("Failed reading of book" + id, ex);
            }
        });
        return book;
    }

    protected Future<Availability> readAvailabilityById(final long id, final BookResponse response) {
        final Future<Availability> availability = dispatcher.future(new Callable<Availability>() {
            @Override
            public Availability call() throws Exception {
                LOG.info("reading availability of book with id {}", id);
                return available.availabilityByBook(id);
            }
        });
        dispatcher.onFailure(availability, new OnFailure() {
            @Override
            public void onFailure(Throwable ex) {
                response.unavailable(ex.getMessage());
                LOG.error("Failed reading availability of book" + id, ex);
            }
        });
        return availability;
    }

    protected Future<Price> readPriceById(final long id, final BookResponse response) {
        final Future<Price> price = dispatcher.future(new Callable<Price>() {
            @Override
            public Price call() throws Exception {
                LOG.info("reading price of book with id {}", id);
                return pricing.priceByBook(id);
            }
        });
        dispatcher.onFailure(price, new OnFailure() {
            @Override
            public void onFailure(Throwable ex) {
                response.unavailable(ex.getMessage());
                LOG.error("Failed reading price of book" + id, ex);
            }
        });
        return price;
    }

    protected Future<Option<BookOffer>> mergeBookOffer(Future<Option<Book>> bookFuture,
                                                       Future<Availability> availabilityFuture,
                                                       Future<Price> priceFuture,
                                                       BookResponse response) {
        return dispatcher.zip(bookFuture, availabilityFuture, priceFuture, new FutureDispatcher.Zipper3Callback<Option<Book>, Availability, Price, Option<BookOffer>>() {
            @Override
            public Option<BookOffer> map(Option<Book> book, Availability availability, Price price) {
                LOG.info("combine book and availability");
                if (book.isDefined()) {
                    return Option.some(new BookOffer(book.get(), availability, price));
                } else {
                    return Option.none();
                }
            }
        });
    }

    protected void handleResult(final long id, Future<Option<BookOffer>> offerFuture, final BookResponse response) {
        dispatcher.onSuccess(offerFuture, new OnSuccess<Option<BookOffer>>() {
            @Override
            public void onSuccess(Option<BookOffer> offer) {
                if (offer.isDefined()) {
                    final String json = offer.get().toJson();
                    response.ok(json);
                    LOG.info("Found book " + json);
                } else {
                    final String msg = "No book found with id " + id;
                    response.notFound(msg);
                    LOG.error(msg);
                }
            }
        });
    }

}
