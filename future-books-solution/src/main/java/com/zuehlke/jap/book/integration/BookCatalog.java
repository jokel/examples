package com.zuehlke.jap.book.integration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collection;

import akka.japi.Option;

public class BookCatalog {
	
	private static final String PATH = "com/zuehlke/jap/book/integration/";
	private static final String FILENAME = "books.txt";
	private static final String ENCODING = "utf8";

	private BufferedReader getReader() throws IOException {
		InputStream in = this.getClass().getClassLoader().getResourceAsStream(PATH + FILENAME);
		if (in == null) throw new IOException("Missing file " + FILENAME);
		Reader reader = new InputStreamReader(in, ENCODING);
		return new BufferedReader(reader);
	}

	/**
	 * Alternatives
	 * - Exception
	 * - Guardian / Null-Object
	 * - Collection (length 0/1)
	 * - null
	 * - Option<Book>
	 */
	public Option<Book> readById(long id) throws IOException {
		try (BufferedReader reader = getReader()) {
			while (reader.ready()) {
				String line = reader.readLine();
				Book book = parse(line);
				if (book.getId() == id) return Option.some(book);
			}
			return Option.none();
		}
	}
	
	public Collection<Book> readAllBooks() throws IOException {
		try (BufferedReader reader = getReader()) {
			Collection<Book> books = new ArrayList<Book>();
			while (reader.ready()) {
				String line = reader.readLine();
				books.add(parse(line));
			}
			return books;
		}
	}
	
	private Book parse(String line) {
		String[] items = line.split("\t");
		return new Book(Long.parseLong(items[0]), items[1], items[2]);
	}

}
