package com.zuehlke.jap.book.service;

/**
 * Service interface to be implemented by all variations of blocking and non-blocking services.
 * Known implementations:
 * - SimpleBookService: Blocking synchronous implementation
 * - Java7BookService: Non-blocking java implementation using akka.japi callback classes
 * - Java8BookService: Non-blocking java implementation using lambdas for akka.japi callbacks
 * - ScalaBookService: Non-blocking scala implementation
 */
public interface BookService {
    void allBooks(BookResponse response);
    void bookById(long id, BookResponse response);
}
