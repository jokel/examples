package com.zuehlke.jap.book.integration;

public class Availability {

	private final long id;
	private final int stock;

	public Availability(long id, int stock) {
		this.id = id;
		this.stock = stock;
	}

	public long getId() {
		return id;
	}

	public int getStock() {
		return stock;
	}

}
