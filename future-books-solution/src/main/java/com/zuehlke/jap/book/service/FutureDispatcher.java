package com.zuehlke.jap.book.service;

import akka.dispatch.*;
import scala.Tuple2;
import scala.concurrent.ExecutionContext;
import scala.concurrent.Future;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Helper class for the java future implementations to hide and centralize the execution context.
 */
public class FutureDispatcher {
	
	private static final int THREADS = Runtime.getRuntime().availableProcessors();

    private final ExecutorService service = Executors.newFixedThreadPool(THREADS);
	private final ExecutionContext ec = ExecutionContexts.fromExecutorService(service);

	
	public <T> Future<T> future(Callable<T> callable) {
		return Futures.future(callable, ec);
	}
	
	public <T> void onSuccess(Future<T> future, OnSuccess<T> onSuccess) {
		future.onSuccess(onSuccess, ec);
	}

    public interface SuccessCallback<T> { void onSuccess(T value) throws Exception; }
    public <T> void onSuccess(Future<T> future, final SuccessCallback<T> callback) {
        future.onSuccess(new OnSuccess<T>(){
            @Override
            public void onSuccess(T value) throws Throwable {
                callback.onSuccess(value);
            }
        }, ec);
    }

    public void onFailure(Future<?> future, OnFailure onFailure) {
		future.onFailure(onFailure, ec);
	}

    public interface FailureCallback { void onFailure(Throwable throwable) throws Exception; }
    public void onFailure(Future<?> future, final FailureCallback callback) {
        future.onFailure(new OnFailure() {
            @Override
            public void onFailure(Throwable throwable) throws Throwable {
                callback.onFailure(throwable);
            }
        }, ec);
    }

    public <T,Z> Future<Z> map(Future<T> future, Mapper<T, Z> mapper) {
        return future.map(mapper, ec);
    }

    public interface MapperCallback<T,Z> { Z map(T item); }
    public <T,Z> Future<Z> map(Future<T> future, final MapperCallback<T, Z> callback) {
        return future.map(new Mapper<T, Z>(){
            @Override
            public Z apply(T parameter) {
                return callback.map(parameter);
            }
        }, ec);
    }

    public <S,T,Z> Future<Z> zip(Future<S> future1, Future<T> future2, Mapper<Tuple2<S,T>, Z> mapper) {
		final Future<Tuple2<S,T>> zip = future1.zip(future2);
		return zip.map(mapper, ec);
	}

    public interface Zipper2Callback<S,T,Z> { Z map(S first, T second); }
    public <S,T,Z> Future<Z> zip(Future<S> future1, Future<T> future2, final Zipper2Callback<S,T,Z> callback) {
        final Future<Tuple2<S,T>> zip = future1.zip(future2);
        return zip.map(new Mapper<Tuple2<S,T>,Z>(){
            @Override
            public Z apply(Tuple2<S,T> tuple) {
                return callback.map(tuple._1(), tuple._2());
            }
        }, ec);
    }

    public interface Zipper3Callback<S,T,U,Z> { Z map(S first, T second, U third); }
    public <S,T,U,Z> Future<Z> zip(Future<S> future1, Future<T> future2, Future<U> future3, final Zipper3Callback<S,T,U,Z> callback) {
        final Future<Tuple2<Tuple2<S,T>,U>> zip = future1.zip(future2).zip(future3);
        return zip.map(new Mapper<Tuple2<Tuple2<S,T>,U>,Z>(){
            @Override
            public Z apply(Tuple2<Tuple2<S,T>,U> tuple) {
                return callback.map(tuple._1()._1(), tuple._1()._2(), tuple._2());
            }
        }, ec);
    }

}
