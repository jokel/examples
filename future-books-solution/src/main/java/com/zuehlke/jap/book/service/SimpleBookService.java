package com.zuehlke.jap.book.service;

import com.zuehlke.jap.book.integration.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collection;
import java.util.Map;

public class SimpleBookService implements BookService {

	protected static Logger LOG = LoggerFactory.getLogger(SimpleBookService.class);
	
	protected BookCatalog catalog = new BookCatalog();
	protected BookAvailability available = new BookAvailability();
    protected BookPrice pricing = new BookPrice();
	protected FutureDispatcher dispatcher = new FutureDispatcher();
	
	/**
	 * PROBLEM: Callbacks with Java7 make code unreadable, too much boiler-plate.
	 * PROBLEM: Executor needs to be used everywhere -> Dispatcher
	 */
	@Override
    public void allBooks(final BookResponse response) {
        try {

            LOG.info("reading books");
            final Collection<Book> books = catalog.readAllBooks();
            LOG.info("reading availability");
            final Map<Long, Availability> availability = available.readAllAvailabilities();
            LOG.info("reading prices");
            final Map<Long, Price> prices = pricing.readAllPrices();
            LOG.info("combine books, availability and price");
            Collection<BookOffer> offers = BookOffer.merge(books, availability, prices);
            LOG.info("book offers to JSON");
            final String json = BookOffer.toJson(offers);
            response.ok(json);
            LOG.info("allBooks responded");

        } catch (IOException ex) {
            response.unavailable(ex.getMessage());
            LOG.error("Failed reading books", ex);
        }
	}

    @Override
    public void bookById(long id, BookResponse response) {
		try {
			
			final Book book = catalog.readById(id).get();
			if (book != null) {
                final Availability availability = available.availabilityByBook(id);
                final Price price = pricing.priceByBook(id);
                BookOffer offer = new BookOffer(book, availability, price);
				final String json = offer.toJson();
				response.ok(json);
				LOG.info("Found book " + json);
			} else {
				final String msg = "No book with id " + id;
				response.notFound(msg);
				LOG.error(msg);
			}
			
		} catch (IOException ex) {
			response.unavailable(ex.getMessage());
			LOG.error("Failed reading of book" + id, ex);
		}
	}

}
