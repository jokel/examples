package com.zuehlke.jap.book.service;

/** 
 * Introduced to avoid dependency from service to rest package. 
 */
public interface BookResponse {

	void ok(String json);

	void notFound(String message);

	void unavailable(String message);

}
