package com.zuehlke.jap.book.service;

import com.zuehlke.jap.book.integration.Availability;
import com.zuehlke.jap.book.integration.Book;
import com.zuehlke.jap.book.integration.Price;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

public class BookOffer {

    private static Logger LOG = LoggerFactory.getLogger(BookOffer.class);
	
	private final Book book;
	private final Availability available;
    private final Price price;
	
	public BookOffer(Book book, Availability available, Price price) {
		this.book = book;
		this.available = available;
        this.price = price;
    }

    public String toJson() {
		return String.format("{title:\"%s\", author:\"%s\", available:%d, price:%.2f}",
                book.getTitle(), book.getAuthor(), available.getStock(), price.getPrice());
	}

    public static Collection<BookOffer> merge(Collection<Book> books,
                                              Map<Long, Availability> availability,
                                              Map<Long, Price> prices) {
        final Collection<BookOffer> offers = new ArrayList<BookOffer>(books.size());
        for (Book book : books) {
            final long id = book.getId();
            Availability available = availability.containsKey(id) ? availability.remove(id) : new Availability(id, 0);
            Price price = prices.remove(id);
            if (price == null) {
                if (available.getStock() != 0) {
                    LOG.warn("Missing price for book {} {} from {} with stock {}", id, book.getTitle(), book.getAuthor(), available.getStock());
                }
                continue;
            }
            offers.add(new BookOffer(book, available, price));
        }
        for (Availability available : availability.values()) {
            if (available.getStock() != 0) {
                LOG.warn("Missing catalog entry for book {} with stock {}", available.getId(), available.getStock());
            }
        }
        return offers;
    }

    public static String toJson(Collection<BookOffer> books) {
        StringBuilder builder = new StringBuilder("[");
        for (BookOffer book : books) {
            builder.append(book.toJson());
            builder.append(",");
        }
        builder.setLength(builder.length() - 1);
        builder.append("]");
        return builder.toString();
    }

}
