package com.zuehlke.jap.option;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.NoSuchElementException;

import org.junit.Ignore;
import org.junit.Test;

import akka.japi.Option;

public class OptionTest {
	
	// no transformation for japi.Option
	// no orElse for japi.Option
	// no orNull for japi.Option

	@Test
	public void optionWrapper() {
		Option<String> some = Option.some("asdf");
		assertTrue(some.isDefined());
		assertFalse(some.isEmpty());
		assertEquals("asdf", some.get());
	}
	
	@Test
	public void noneWrapper() {
		Option<String> none = Option.none();
		assertFalse(none.isDefined());
		assertTrue(none.isEmpty());
	}
	
	@Test
	public void someNullable() {
		Option<String> some = Option.option("asdf");
		assertEquals("asdf", some.get());
	}

	@Test
	public void noneNullable() {
		Option<String> none = Option.option(null);
		assertTrue(none.isEmpty());
	}
	
	// creates some with null!
	@Ignore @Test(expected = NoSuchElementException.class)
	public void someWithNull() {
		Option.some(null);
	}
	
	@Test(expected = NoSuchElementException.class)
	public void getFromNone() {
		Option.none().get();
	}
	
}
