package com.zuehlke.jap.book.service;

import org.junit.Test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertThat;

public class BookServiceTest {
	
	private final BookService bookService = new Java7BookService();
	private final MockResponse response = new MockResponse();
	
	@Test
	public void aBookTitle() throws InterruptedException {
		bookService.bookById(1, response);
        Thread.sleep(100L);
		assertThat(response.json, containsString("title:\"Agile Software Development\""));
	}

	@Test
	public void aBookAuthor() throws InterruptedException {
		bookService.bookById(2, response);
        Thread.sleep(100L);
		assertThat(response.json, containsString("author:\"Martin Fowler\""));
	}

	@Test
	public void unknownBook() throws InterruptedException {
		bookService.bookById(-1, response);
        Thread.sleep(100L);
		assertThat(response.notFoundMsg, containsString("with id -1"));
	}
	
	/**
	 * PROBLEM: Async behaviour, test does not know when finished (FIXME sleep) 
	 */
	@Test
	public void allBooks() throws InterruptedException {
		bookService.allBooks(response);
		Thread.sleep(100L);
		assertThat(response.json, containsString("title:\"Agile"));
		assertThat(response.json, containsString("author:\"Robert C. Martin\""));
		assertThat(response.json, containsString("available:100"));
		assertThat(response.json, containsString("price:22.95"));
		assertThat(response.json, containsString("title:\"Refactoring\""));
		assertThat(response.json, containsString("author:\"Martin Fowler\""));
		assertThat(response.json, containsString("available:85"));
		assertThat(response.json, containsString("price:45.45"));
	}

}
